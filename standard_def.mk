#### Common variables for Makefiles

## Directories
ROOT=$(shell pwd)
DIR_BIN=bin
DIR_SRC=src
DIR_OBJ=obj
DIR_LIB=lib
DIR_HDR=includes
DIR_DOC=doc
DIR_IN=libCalendar

## Compilation
GXX=g++
# Flags for linking
CFLAGS=-L$(DIR_LK)/$(DIR_LIB)  -L/opt/pococpp/lib/ -lPocoFoundation -lPocoNet -lPocoJSON -lPocoUtil  -lsqlite3 -lmysqlclient -lpthread -lz -lm -lssl -lcrypto 
# Flags for objects
CFLAGS_OBJ=-I$(ROOT)/$(DIR_HDR) -I$(DIR_LK)/$(DIR_HDR) -I/opt/pococpp/include/  -I/usr/include/mysql -O2 -Wall -Wextra -std=c++14 
# Flags for libraries objects
CFLAGS_DYNAMIC_LIBS_OBJ=-fPIC
# Flags for libraries linkins
CFLAGS_DYNAMIC_LIBS=-shared

#-L$(DIR_LK)/$(DIR_LIB)

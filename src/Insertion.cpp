#include "Insertion.hh"
#include "Utils.hpp"

Insertion::Insertion(){

}


// Insertion::Insertion(std::string fileName, const char * dbfile, std::string newIdPerson){
//   logProcess = new LogProc(fileName);
//   db = new Database(dbfile);
//   set_idPerson(newIdPerson);
//   newFile_InsertionTreatment();
// }

Insertion::~Insertion(){
}

void Insertion::init(std::string dbpath){
  db.open(dbpath);
}
void Insertion::clear(){
  requests.clear();
  db.close();
}
std::string Insertion::build_req_app(std::string app_){
  std::string req_app="INSERT INTO application (name) VALUES ('";
  req_app+=app_;
  req_app+="');";
  return req_app;
}
std::string Insertion::build_req_dev_type(std::string type_){
  std::string req_dev_type="INSERT INTO device_type (type) VALUES ('";
  req_dev_type+=type_;
  req_dev_type+="');";
  return req_dev_type;
}
std::string Insertion::build_req_dev_loc(std::string loc_){
  std::string req_dev_loc="INSERT INTO device_location (location) VALUES ('";
  req_dev_loc+=loc_;
  req_dev_loc+="');";
  return req_dev_loc;
}
std::string Insertion::build_req_dev(std::string loc_,std::string type_){
  std::string req_dev="INSERT INTO device (id_location,id_type) SELECT * FROM (SELECT device_location.id , device_type.id FROM device_location, device_type WHERE device_location.location = '";
  req_dev+=loc_;
  req_dev+="' and device_type.type = '";
  req_dev+=type_;
  req_dev+="')";
  return req_dev;
}
std::string Insertion::build_req_date_time(std::string date_time_){
  std::string req_date_time="INSERT INTO date_time (dt) VALUES ('";
  req_date_time+=date_time_;
  req_date_time+="');";
  return req_date_time;
}
std::string Insertion::build_req_log(std::string date_time_,std::string loc_,std::string type_,std::string status_){
  std::string req_log="INSERT INTO log (id_date_time,id_device,status) SELECT * FROM (SELECT date_time.id, device.id, '";
  req_log+=status_;
  req_log+="' FROM date_time, device, device_location, device_type WHERE date_time.dt='";
  req_log+=date_time_;
  req_log+="' AND device.id_location=device_location.id and device.id_type=device_type.id AND device_type.type = '";
  req_log+=type_;
  req_log+="' AND device_location.location = '";
  req_log+=loc_;
  req_log+="');";
  return req_log;
}
std::string Insertion::build_req_coresp(std::string date_time_,std::string loc_,std::string type_,std::string app_){
  std::string coresp="INSERT INTO correspond (id_log,id_app) SELECT * FROM ( SELECT log.id, application.id FROM application, log, date_time, device, device_location, device_type WHERE log.id_date_time=date_time.id AND log.id_device=device.id AND device.id_location=device_location.id AND device.id_type=device_type.id AND application.name='";
  coresp+=app_;
  coresp+="' AND device_type.type='";
  coresp+=type_;
  coresp+="' AND device_location.location='";
  coresp+=loc_;
  coresp+="' AND date_time.dt='";
  coresp+=date_time_;
  coresp+="');";
  return coresp;
}
std::vector<std::string> Insertion::insert_line(std::string app_,std::string type_,std::string location_,std::string status_,std::string date_time_){
  std::vector<std::string> req_one_log;
  req_one_log.emplace_back(build_req_app(app_));
  req_one_log.emplace_back(build_req_dev_type(type_));
  req_one_log.emplace_back(build_req_dev_loc(location_));
  req_one_log.emplace_back(build_req_dev(location_,type_));
  req_one_log.emplace_back(build_req_date_time(date_time_));
  req_one_log.emplace_back(build_req_log(date_time_,location_,type_,status_));
  req_one_log.emplace_back(build_req_coresp(date_time_,location_,type_,app_));

  // db.insert(build_req_app(app_));
  // db.insert(build_req_dev_type(type_));
  // db.insert(build_req_dev_loc(location_));
  // db.insert(build_req_dev(location_,type_));
  // db.insert(build_req_date_time(date_time_));
  // db.insert(build_req_log(date_time_,location_,type_,status_));
  // db.insert(build_req_coresp(date_time_,location_,type_,app_));
  
  return req_one_log;
  
  // std::string req_app=build_req_app(app_);
  // std::string req_dev_type=build_req_dev_type(type_);
  // std::string req_dev_loc=build_req_dev_loc(location_);
  // std::string req_dev=build_req_dev(location_,type_);
  // std::string req_date_time=build_req_date_time(date_time_);
  // std::string req_log=build_req_log(date_time_,location_,type_,status_);
  // std::string coresp=build_req_coresp(date_time_,location_,type_,app_);

  // db.query(req_app);
  // db.query(req_dev_type);
  // db.query(req_dev_loc);
  // db.query(req_dev);
  // db.query(req_date_time);
  // db.query(req_log);
  // db.query(coresp);
  
  // if(!app_.compare("DoorAlarm"))
  //std::cout<<app_<<" "<<type_<<" "<<location_<<" "<<status_<<" "<<date_time_<<endl;
  //std::cout<<req_dev_type<<" "<<std::endl;
}

void Insertion::create_requests_db(std::string app_,std::string type_,std::string location_,std::string status_,std::string date_time_){
  std::vector<std::vector<std::string>> file_content;
  // for(size_t i(0);i<logs.size();++i){
    //display="echo -en \"\r";
    //std::cout<<logs[i][0]<<" "<<logs[i][1]<<" "<<logs[i][2]<<" "<<logs[i][3]<<" "<<logs[i][4] <<std::endl;
    file_content.emplace_back(insert_line(app_,type_,location_,status_,date_time_));
    //insert_line(logs[i][0],logs[i][1],logs[i][2],logs[i][3],logs[i][4]);
    // display+=Utils::toString<size_t>(i);
    // display+="/";
    // display+=Utils::toString<size_t>(logs.size());
    // display+="\"";
    // std::system(display.c_str());
    //}
  for(size_t i(0);i<file_content.size();++i){
    for(size_t j(0);j<file_content[i].size();++j){
      //std::cout<<file_content[i][j]<<std::endl;
      db.insert(file_content[i][j]);
    }
  }
  
}
void Insertion::exec_requests(){
  std::string display="";
  size_t size=requests.size();
  
  if(!requests.empty()){
    display="echo -en \"\r";
    display+=Utils::toString<size_t>(1);
    display+="/";
    display+=Utils::toString<size_t>(size);
    display+="\"";
    std::system(display.c_str());
    db.insert(requests[0]);
  }
  for(size_t i(1);i<requests.size();++i){     
    display="echo -en \"\r";
    display+=Utils::toString<size_t>(i+1);
    display+="/";
    display+=Utils::toString<size_t>(size);
    display+="\"";
    std::system(display.c_str());
    db.insert(requests[i]);
  }
}
void Insertion::get_file_content(std::string filename){
  // file.setFileName(filename);

  
  // std::string line_=file.readLine();
  // //std::cout<<line_<<std::endl;

  // line.setLine(line_);

  // while(line_.compare("NONEEOFLOL")){
  //   line_=file.readLine();
  //   line.setLine(line_);
  //   if(line.isLogLine()){
  //     //std::cout<<line_<<std::endl;
  //     line.InfoRecov();
  //     logs.emplace_back(line.getInfoArray());
  //   }
  // }
  //create_requests_db();
  //std::cout<<line_<<std::endl;


  //file.closeFile();
  for(size_t i(0);i<logs.size();++i)
    logs[i].clear();
  logs.clear();
  
}


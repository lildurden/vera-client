#include "time.hh"
#include <Utils.hpp>
#include <cmath>
#include <cfenv>

#include <iomanip>
#include <sstream>

#include <iostream>

Time::Time(){
  parse("1970-01-01 01:00:00.000");
  //std::istringstream ss("1970-01-01 01:00:00.000");
  //ss >> std::get_time(&timestmp, "%Y-%m-%d %H:%M:%S");
  //timestmp.tm_isdst=1;
  //timestamp=0;
  //msec=0;
}
Time::Time(const Time & time){
  affect(time);
}
Time::Time(std::string time_full){
  parse(time_full);

}
Time::Time(std::string days,std::string times){
  //std::cout<<days<<" "<<times<<std::endl;
  parse(days,times);
}
Time::Time(std::time_t timestmp_){
  timestmp=*std::localtime(&timestmp_);
  to_timestamp();
  msec=0;
}

Time::~Time(){
}

unsigned long Time::get_timestamp(){
  return timestamp;
}

bool Time::greater(const Time& a){
  return timestamp>a.timestamp;
}
bool Time::less(const Time& a){
  return timestamp<a.timestamp;
}
bool Time::equal(const Time& a){
  //std::cout<<to_string()<<" "<<timestamp<<" "<<msec<<std::endl;
  //std::cout<<a.to_string()<<" "<<timestamp<<" "<<msec<<std::endl;
  if(timestamp==a.timestamp)
    return msec==a.msec;
  return false;
}
bool Time::greater_equal(const Time& a){
  if(timestamp==a.timestamp)
    return msec>=a.msec;
  return timestamp>=a.timestamp;
}
bool Time::less_equal(const Time& a){
  if(timestamp==a.timestamp)
    return msec<=a.msec;
  return timestamp<=a.timestamp;
}

Time& Time::affect(const Time& time){
  dailytimstmp = time.dailytimstmp;
  timestmp.tm_hour = time.timestmp.tm_hour;
  timestmp.tm_min = time.timestmp.tm_min;
  timestmp.tm_sec = time.timestmp.tm_sec;
  timestmp.tm_year = time.timestmp.tm_year;
  timestmp.tm_mon = time.timestmp.tm_mon ;
  timestmp.tm_mday = time.timestmp.tm_mday;
  timestmp.tm_isdst=-1;
  timestamp=time.timestamp;
  msec=time.msec;
  return *this;
}

// unsigned int Time::plus(const Time& a){ 
//   return 0;
// }

unsigned int Time::minus(const Time& a){
  //std::cout<<timestamp<<" "<<a.timestamp<<std::endl;
  if(timestamp>a.timestamp)
    return (timestamp-a.timestamp);
  else return (a.timestamp-timestamp);
}
unsigned int Time::minus_day_(const Time& a){
  if(timestamp>a.timestamp)
    return (timestamp-a.timestamp)/3600/24;
  else
    return (a.timestamp-timestamp)/3600/24;
}

void Time::to_timestamp(){
  dailytimstmp = timestmp.tm_hour*3600+timestmp.tm_min*60+timestmp.tm_sec;

  std::time_t truc=mktime(&timestmp);
  timestamp=truc;
}
void Time::to_time(){}

void Time::parse(std::string time_full){
  
  
  std::istringstream ss(time_full);
  ss >> std::get_time(&timestmp, "%Y-%m-%d %H:%M:%S");
  size_t finsec=time_full.find_last_of(".");
  msec=Utils::stringTo<unsigned int>(time_full.substr(finsec,time_full.size()-finsec));
  timestmp.tm_isdst=-1;
  to_timestamp();

  //std::cout<<"Full parse "<<time_full<< " "<<to_string()<<std::endl;
  
}
void Time::parse(std::string days,std::string times){
  days.append(" ");
  days.append(times);
  parse(days);
  // std::istringstream ss(days);
  // ss >> std::get_time(&timestmp, "%Y-%m-%d %H:%M:%S");
  // timestmp.tm_isdst=1;
  // to_timestamp();
}
unsigned int Time::get_daily_timestamp(){
  return dailytimstmp;
}
std::string Time::to_string()const {
  std::string out=get_day_str();
  out+='\t';

  //std::string out(buffer_date);

  //out+='\t';
  // out+=buffer_time;

  /*std::string */out+=get_hour_str();
  // out+='\t';
  //std::string out="";
  
  // out+=Utils::toString<unsigned int>(/*dailytimstmp*/timestamp);
  out+='\t';
  return out;
}
std::string Time::get_day_str() const{
  char buffer_date[80];
  strftime(buffer_date,80,"%F" ,&timestmp);
  std::string day(buffer_date);
  return day;
}
std::string Time::get_hour_str() const{
  char buffer_time[80];
  strftime(buffer_time,80,"%T",&timestmp);
  std::string hour(buffer_time);
  return hour;
}

bool Time::operator>(const Time& a){
  return greater(a);
}
bool Time::operator<(const Time& a){
  return less(a);
}
bool Time::operator==(const Time& a){
  return equal(a);
}
bool Time::operator>=(const Time& a){
  return greater_equal(a);
}
bool Time::operator<=(const Time& a){
  return less_equal(a);
}
Time& Time::operator=(const Time& a){
  return affect(a);
}
Time& Time::operator=(std::string time_full){
  const Time a(time_full);
  return affect(a);
}
unsigned int Time::operator-(const Time& a){
  return minus(a);
}
// unsigned int Time::operator+(const Time& a){
//   return plus(a);
// }
// Time& Time::operator+=(const Time& a){
//   return *this;
// }
// Time& Time::operator-=(const Time& a){
//   return *this;
// }

std::ostream& operator<<(std::ostream& out, const Time& a){
  out<<a.to_string();
  return out;
}

#include "request.hh"


void Request::init(std::string url){
  Poco::URI uri(url);
  std::cout<<uri.getHost()<<" lol "<<url<<std::endl;

  cli_session.setHost(uri.getHost());
  cli_session.setPort(uri.getPort());

  request.setMethod(Poco::Net::HTTPRequest::HTTP_GET);
  request.setURI(get_path(uri.getPathAndQuery()));
  request.setVersion(Poco::Net::HTTPMessage::HTTP_1_0);
}

Poco::Dynamic::Var Request::send_request(){
  cli_session.sendRequest(request);
  std::istream& rs = cli_session.receiveResponse(response);
  std::cout<<response.getStatus()<<" "<<response.getReason()<<std::endl;
  Poco::JSON::Parser parser;
  Poco::Dynamic::Var results=parser.parse(rs);
  return results;
}

std::string Request::get_path(std::string path_){
  std::string path=path_;
  if (path.empty()) path = "/";
  return path;
}

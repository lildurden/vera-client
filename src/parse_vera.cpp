#include "parse_vera.hh"

void Parse_Vera::init(std::string uri){
  req.init(uri);
  exec_request();
}
void Parse_Vera::set_all(){
  set_rooms();
  set_categories();
  set_devices();
}

void Parse_Vera::exec_request(){
  json_results = req.send_request().extract<Poco::JSON::Object::Ptr>();
}

void Parse_Vera::set_rooms(){
  Poco::Dynamic::Var rooms = json_results->get("rooms");
  Poco::JSON::Array::Ptr json_rooms =
    rooms.extract<Poco::JSON::Array::Ptr>();
  for(size_t i(0);i<json_rooms->size();++i){
    Poco::JSON::Object::Ptr room=json_rooms->getObject(i);
    Poco::Dynamic::Var elem=room->get("name");
    std::string val = elem.toString();
    elem=room->get("id");
    int id=elem.extract<int>();
    std::shared_ptr<Room> shd_room(new Room(val,id));
    rooms_map.insert(std::pair<int,std::shared_ptr<Room>>(id,shd_room));
    //rooms_map[id]=Room(val,id);
  }
}

void Parse_Vera::set_categories(){
  Poco::Dynamic::Var cats = json_results->get("categories");
  Poco::JSON::Array::Ptr json_cats =
    cats.extract<Poco::JSON::Array::Ptr>();
  for(size_t i(0);i<json_cats->size();++i){
    Poco::JSON::Object::Ptr cats=json_cats->getObject(i);
    Poco::Dynamic::Var elem=cats->get("name");
    std::string val = elem.toString();
    elem=cats->get("id");
    int id=elem.extract<int>();
    std::shared_ptr<Category> shd_category(new Category(val,id));
    categories_map.insert(std::pair<int,std::shared_ptr<Category>>(id,shd_category));
  }
}

void Parse_Vera::set_devices(){
  //std::cout<<"devices"<<std::endl;
  Poco::Dynamic::Var devices = json_results->get("devices");
  Poco::JSON::Array::Ptr json_devices =
    devices.extract<Poco::JSON::Array::Ptr>();
  Poco::JSON::Object::Ptr device;
  Poco::Dynamic::Var elem;
  int category; 
  for(size_t i(0);i<json_devices->size();++i){
    device=json_devices->getObject(i);
    elem=device->get("category");
    category=elem.extract<int>();
   
    switch(category){
    case 1:
      break;
    case 2: break;
    case 3: break;
    case 4:
      set_cat4(device);
      break;
    case 16:
      // set_cat16(device);
      break;
    case 17:
      set_cat17(device);
      break;
    case 18:
      set_cat18(device);
      break;
    default: break;
    }    
    Poco::Dynamic::Var elem=device->get("name");
    std::string val = elem.toString();

    
  }
}

void Parse_Vera::set_cat4(Poco::JSON::Object::Ptr json_device_){
  Poco::Dynamic::Var elem;
  int subcategory;
  elem=json_device_->get("subcategory");
  subcategory=elem.extract<int>();
  
  switch(subcategory){
  case 1:
    set_cat4_3(json_device_,"ContactSensor");
    break;
  case 2: break;
  case 3:
    set_cat4_3(json_device_,"MotionDetector");
    break;
  case 4: break;
  case 5: break;
  case 6: break;
  default:break;
  }
}

void Parse_Vera::set_cat16(Poco::JSON::Object::Ptr json_device_){
}
void Parse_Vera::set_cat17(Poco::JSON::Object::Ptr json_device_){
  Poco::Dynamic::Var elem;
  elem=json_device_->get("id");
  int id=elem.extract<int>();
  elem=json_device_->get("room");
  int room=elem.extract<int>();

  elem=json_device_->get("parent");
  int parent=elem.extract<int>();
  elem=json_device_->get("temperature");
  std::string status = elem.toString();
  elem=json_device_->get("name");
  std::string name = elem.toString();
    	
  // std::shared_ptr<Category> cat_ptr=std::make_shared<Category>(categories_map.at(4));
  // std::shared_ptr<Room> room_ptr=room==0?nullptr:std::make_shared<Room>(rooms_map.at(room));

  sensors_map[id]=Sensor(categories_map.at(17),
  			 room==0?nullptr:rooms_map.at(room),
  			 parent!=1?std::make_shared<Sensor>(sensors_map.at(parent)):nullptr,
  			 id,
  			 status,
  			 0,name);
}
void Parse_Vera::set_cat18(Poco::JSON::Object::Ptr json_device_){
  Poco::Dynamic::Var elem;
  elem=json_device_->get("id");
  int id=elem.extract<int>();
  elem=json_device_->get("room");
  int room=elem.extract<int>();

  elem=json_device_->get("parent");
  int parent=elem.extract<int>();
  elem=json_device_->get("light");
  std::string status = elem.toString();
  elem=json_device_->get("name");
  std::string name = elem.toString();
    	
  // std::shared_ptr<Category> cat_ptr=std::make_shared<Category>(categories_map.at(4));
  // std::shared_ptr<Room> room_ptr=room==0?nullptr:std::make_shared<Room>(rooms_map.at(room));

  sensors_map[id]=Sensor(categories_map.at(18),
  			 room==0?nullptr:rooms_map.at(room),
  			 parent!=1?sensors_map.find(parent)==sensors_map.end()?nullptr:std::make_shared<Sensor>(sensors_map.at(parent)):nullptr,
  			 id,
  			 status,
  			 0,name);
  
}
void Parse_Vera::set_cat4_3(Poco::JSON::Object::Ptr json_device_,std::string name_){
  Poco::Dynamic::Var elem;
  elem=json_device_->get("id");
  int id=elem.extract<int>();
  elem=json_device_->get("room");
  int room=elem.extract<int>();      
  elem=json_device_->get("parent");
  int parent=elem.extract<int>();
  if(parent==1){

    elem=json_device_->get("tripped");
    std::string status = elem.toString();

    elem=json_device_->get("lasttrip");
    std::string timestamp = elem.toString();
    elem=json_device_->get("name");
    std::string name = elem.toString();
    //std::cout<<categories_map.size()<<" "<<rooms_map.size()<<std::endl;
    	
    //std::shared_ptr<Category> cat_ptr=std::make_shared<Category>(categories_map.at(4));
    //std::shared_ptr<Room> room_ptr=room==0?nullptr:std::make_shared<Room>(rooms_map.at(room));
    //std::cout<<name<<std::endl;
    
    sensors_map[id]=Sensor(categories_map.at(4),
			   room==0?nullptr:rooms_map.at(room),
			   nullptr,
			   id,
			   status,
			   Utils::stringTo<unsigned int>(timestamp),name_);
  }
}
// void Parse_Vera::set_cat4_1(Poco::JSON::Object::Ptr json_device_){
//   Poco::Dynamic::Var elem;
//   elem=json_device_->get("id");
//   int id=elem.extract<int>();
//   elem=json_device_->get("room");
//   int room=elem.extract<int>();

//   elem=json_device_->get("parent");
//   int parent=elem.extract<int>();
//   elem=json_device_->get("tripped");
//   std::string status = elem.toString();
//   elem=json_device_->get("lasttrip");
//   std::string timestamp = elem.toString();
    	
//   std::shared_ptr<Category> cat_ptr=std::make_shared<Category>(categories_map.at(4));
//   std::shared_ptr<Room> room_ptr=room==0?nullptr:std::make_shared<Room>(rooms_map.at(room));

//   sensors_map[id]=Sensor(cat_ptr,
//   			 room_ptr,
//   			 nullptr,
//   			 id,
//   			 status,
//   			 Utils::stringTo<unsigned int>(timestamp));
// }


// void Parse_Vera::update_devices(){
//   json_results = req.send_request().extract<Poco::JSON::Object::Ptr>();
//   Poco::Dynamic::Var devices = json_results->get("devices");
//   Poco::JSON::Array::Ptr json_devices =
//     devices.extract<Poco::JSON::Array::Ptr>();
//   Poco::JSON::Object::Ptr device;
//   Poco::Dynamic::Var elem;
//   for(size_t i(0);i<json_devices->size();++i){
//     device=json_devices->getObject(i);
//     elem=device->get("id");
//     int id=elem.extract<int>();
//     elem=device->get("tripped");
//     std::string status = elem.toString();

//     elem=device->get("parent");
//     int parent=elem.extract<int>();

//     elem=device->get("lasttrip");
//     std::string timestamp = elem.toString();
    
//     //sensors_map.at(id).set_status(status,(parent==1?Utils::stringTo<unsigned int>(timestamp):sensors_map[parent].get_last_trip()));
    
//   }
// }

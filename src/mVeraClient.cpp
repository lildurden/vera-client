#include <iostream>
#include "parse_vera.hh"

int main(int argc, char** argv){
 
  Parse_Vera pvera;

  pvera.init(argv[1]);
  pvera.set_all();

  while(1){
    pvera.exec_request();
    pvera.set_devices();
    sleep(10);
  }
 

  // while(1){
  //   Poco::JSON::Object::Ptr json_results =
  //     req.send_request().extract<Poco::JSON::Object::Ptr>();

  //   Poco::Dynamic::Var devices = json_results->get("devices");
  //   Poco::JSON::Array::Ptr json_devices =
  //     devices.extract<Poco::JSON::Array::Ptr>();

  //   for(size_t i(0);i<json_devices->size();++i){
  //     Poco::JSON::Object::Ptr device=json_devices->getObject(i);

  //     Poco::Dynamic::Var elem=device->get("name");
  //     std::string val = elem.toString();

  //     elem=device->get("id");
  //     int id=elem.extract<int>();
  //     if(val.find("MotionD")!=std::string::npos){
  // 	elem=device->get("room");
  // 	int room=elem.extract<int>();
  // 	elem=device->get("category");
  // 	int category=elem.extract<int>();
  // 	elem=device->get("parent");
  // 	int parent=elem.extract<int>();
  // 	elem=device->get("tripped");
  // 	std::string status = elem.toString();
  // 	elem=device->get("lasttrip");
  // 	std::string timestamp = elem.toString();
    	
  // 	sensors_map.at(id).set_status(status,Utils::stringTo<unsigned int>(timestamp));
	
  //     }
  //   }
    
  //   sleep(2);
  // }
  return 0;

}

/** \mainpage : 
 * \author adcarter
 * \version : 0.0 $
 * \date : mer. déc.  9 12:55:54 CET 2015 $
 * \section intro_sec Introduction
 * Contact: acarteron@openmailbox.org
 * Created on: mer. déc.  9 12:55:54 CET 2015
 *
 * \section install_sec Installation
 *
 * This is f***** easy
 *
 * \subsection step1 Step 1 : Compile
 *
 *  Run this command to the project root
 *  <br/>
 *  <code>make</code>
 *
 * \subsection step2 Step 2 : Launch
 *
 *  Compiled program can be found in the subfolder <code>./bin</code>, it is called <code></code>.
 * 
 *  \subsection step3 Step 3 : Otherwise
 *  
 *  You can read the file <code>README.RTFM</code> if it exists
 */

/*





    {
        "full": 1,
        "version": "*1.7.730*",
        "model": "MiCasaVerde VeraLite",
        "zwave_heal": 1,
        "temperature": "C",
        "skin": "mios",
        "serial_number": "35110516",
        "fwd1": "vera-us-oem-relay31.mios.com",
        "fwd2": "vera-us-oem-relay12.mios.com",
        "mode": 1,
        "sections":
        [
            {
                "name": "My Home",
                "id": 1
            }
        ],
        "rooms":
        [
            {
                "name": "Entrance",
                "id": 1,
                "section": 1
            },
            {
                "name": "Kitchen",
                "id": 2,
                "section": 1
            }
        ],
        "scenes":
        [
        ],
        "devices":
        [
            {
                "name": "Light Sensor",
                "altid": "m3",
                "id": 36,
                "category": 18,
                "subcategory": 0,
                "room": 1,
                "parent": 4,
                "light": "13"
            },
            {
                "name": "Light Sensor 1",
                "altid": "m3",
                "id": 53,
                "category": 18,
                "subcategory": 0,
                "room": 0,
                "parent": 51,
                "light": "11"
            },
            {
                "name": "Motion Sensor",
                "altid": "b12",
                "id": 34,
                "category": 4,
                "subcategory": 3,
                "room": 1,
                "parent": 4,
                "armed": "0",
                "armedtripped": "0"
            },
            {
                "name": "Motion Sensor 1",
                "altid": "8",
                "id": 51,
                "category": 4,
                "subcategory": 3,
                "parent": 1,
                "armed": "0",
                "armedtripped": "0",
                "batterylevel": "100",
                "room": 2,
                "lasttrip": "1449742749",
                "tripped": "0",
                "light": "11",
                "temperature": "23.1",
                "state": -1,
                "comment": ""
            },
            {
                "name": "Motion Sensor 2",
                "altid": "b12",
                "id": 54,
                "category": 4,
                "subcategory": 3,
                "room": 0,
                "parent": 51,
                "armed": "0",
                "armedtripped": "0"
            },
            {
                "name": "MotionD",
                "altid": "3",
                "id": 4,
                "category": 4,
                "subcategory": 3,
                "room": 1,
                "parent": 1,
                "armed": "0",
                "armedtripped": "0",
                "lasttrip": "1449749212",
                "tripped": "0",
                "batterylevel": "99",
                "light": "13",
                "temperature": "24.5",
                "state": -1,
                "comment": ""
            },
            {
                "name": "Temperature Sensor",
                "altid": "m1",
                "id": 35,
                "category": 17,
                "subcategory": 0,
                "room": 1,
                "parent": 4,
                "temperature": "24.5"
            },
            {
                "name": "Temperature Sensor 1",
                "altid": "m1",
                "id": 52,
                "category": 17,
                "subcategory": 0,
                "room": 0,
                "parent": 51,
                "temperature": "23.1"
            }
        ],
        "categories":
        [
            {
                "name": "Sensor",
                "id": 4
            },
            {
                "name": "Temperature Sensor",
                "id": 17
            },
            {
                "name": "Light Sensor",
                "id": 18
            }
        ],
        "ir": 0,
        "irtx": "",
        "loadtime": 1449739406,
        "dataversion": 739401371,
        "state": -1,
        "comment": ""
    }


*/

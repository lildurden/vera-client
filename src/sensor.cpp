#include "sensor.hh"
#include "Insertion.hh"
#include "time.hh"
#include <iostream>
#include <cstdlib>

Sensor::Sensor(){
  category=nullptr; room=nullptr; parent=nullptr;
  id=-1;
  
  status="lol";
  timestamp=0;
}
Sensor::Sensor(std::shared_ptr<Category> category_,
	       std::shared_ptr<Room> room_,
	       std::shared_ptr<Sensor> parent_,
	       int id_,
	       std::string status_,
	       unsigned int timestamp_,std::string name_){
  category=category_; room=room_; parent=parent_;
  id=id_;
  name=name_;
  status=status_;
  //if(parent==nullptr)
  timestamp=timestamp_;
  //std::cout<<"constr "<<status<<" at "<<timestamp_<<std::endl;

}
Sensor::~Sensor(){
}

void Sensor::set_status(std::string status_, unsigned int timestamp_){
  // std::cout<<"status charged to "<<status<<" at "<<timestamp<<std::endl;
  // std::cout<<"status charged to "<<status_<<" at "<<timestamp_<<std::endl;
  Insertion insert;
  insert.init("./database/8000.db");
  if(status!=status_){
    status=status_;
    timestamp=timestamp_;


    std::time_t tim=(timestamp);
    Time time(tim);

    std::string time_full=time.get_day_str();
    time_full+=" ";
    time_full+=time.get_hour_str();
    time_full+=".000";
    std::string status_="";
    if(!name.compare("MotionDetector"))
      status_=(!status.compare("1")?"true":"false");
    else{
      if(!name.compare("ContactSensor"))
	status_=(!status.compare("0")?"true":"false");
      else{
	status_=status;
      }
    }
    if(timestamp!=0){
      std::cout<<"Activities "<<name<<" "<<room->get_name()<<" "<<status_<<" "<<time_full<<std::endl;
      insert.create_requests_db("Activities",name,room->get_name(),status_,time_full);
    }
  }else{
    if(timestamp!=timestamp_){
      timestamp=timestamp_;
      //std::cout<<name<<" did not changed at "<<timestamp<<std::endl;
    }
  }
  insert.clear();
}

std::string Sensor::get_status(){
  return status;
}
unsigned int Sensor::get_last_trip(){
  if(parent==nullptr)
    return timestamp;
  else return parent->get_last_trip();
}
bool Sensor::compare(const Sensor& sensor_){
  return id==sensor_.id;
}
int Sensor::get_id(){
  return id;
}
Sensor& Sensor::operator=(const Sensor & sensor_){
  //std::cout<<"ids "<<id<<" "<<sensor_.id<<std::endl;
  if(id!=sensor_.id&&id!=-1)
    return *this;
  category=sensor_.category;
  room=sensor_.room;
  parent=sensor_.parent;
  name=sensor_.name;
  set_status(sensor_.status,
	     parent==nullptr?sensor_.timestamp:sensor_.parent->get_last_trip());
  return *this;
}

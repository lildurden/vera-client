#include "elements.hh"

Elements::Elements(std::string name_,unsigned int id_){
  id=id_;
  name=name_;
}
Elements::~Elements(){
}
unsigned int Elements::get_id(){
  return id;
}
std::string Elements::get_name(){
  return name;
}
Elements& Elements::operator=(const Elements& elements_){
  id=elements_.id;
  name=elements_.name;
  return *this;
}

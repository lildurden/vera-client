#include "Database.hh"
#include <iostream>
#include "createdb.hh"
Database::Database(){
  openDB=false;
  base=NULL;
}

Database::Database(std::string filename){
  openDB=false;
  base=NULL;
  open(filename);
}

Database::~Database(){
}

bool Database::create_db(std::string filename){
  char *zErrMsg = 0;
  if(sqlite3_open_v2(filename.c_str(),&base,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE ,NULL) == SQLITE_OK){
    if(base!=NULL){
      sqlite3_exec(base, "PRAGMA journal_mode = MEMORY", NULL, NULL, &zErrMsg);
      std::vector<std::string> req=Create_DB::get_create_requests();
      for(size_t i(0);i<req.size();++i)
	insert(req[i]);
      return true;
    }
    else
      std::cerr<<"ur database seems to be so null"<<std::endl;
  }
  else{
    std::cerr<<"cant open database, maybe cuz something sux"<<std::endl;
    close();
  }
  return false;
}

bool Database::open(std::string filename){
  char *zErrMsg = 0;
  if(!filename.empty()){
    if(!openDB){
      if(sqlite3_open_v2(filename.c_str(),&base,SQLITE_OPEN_READWRITE ,NULL) == SQLITE_OK){
	if(base!=NULL){
	  sqlite3_exec(base, "PRAGMA journal_mode = MEMORY", NULL, NULL, &zErrMsg);
	  return true;
	}else
	  std::cerr<<"ur database seems to be so null"<<std::endl;
      }
      else{
	std::cerr<<"cant open database, it doesn't exists, so i'll try to create one"<<std::endl;
	close();
	if(!create_db(filename))
	  std::cerr<<"i'm sorry i really tried"<<std::endl;
      }
    }else
      std::cerr<<"cant open database, maybe cuz its already open dumbass"<<std::endl;
  }
  else
    std::cerr<<"wont open database, cuz ur filename sux"<<std::endl;
  return false;   
}

void Database::insert(std::string query){
  char *zErrMsg = 0;
  sqlite3_exec(base, "BEGIN TRANSACTION", NULL, NULL, &zErrMsg);
  
  int rc = sqlite3_exec(base, query.c_str(), callback, 0, &zErrMsg);
  if( rc != SQLITE_OK ){
    //std::cerr<<"SQL error: "<< zErrMsg<<std::endl;
    sqlite3_free(zErrMsg);
  }else{
    //std::cerr<< "Records created successfully"<<std::endl;
  }
  sqlite3_exec(base, "END TRANSACTION", NULL, NULL, &zErrMsg);
}


std::vector<std::vector<std::string>> Database::query(std::string query){

  sqlite3_stmt *statement;
  std::vector<std::vector<std::string> > results;
  std::string error = "";
  std::vector<std::string> values;
  std::string val="";

  
  if(sqlite3_prepare_v2(base, query.c_str(), -1, &statement, 0) == SQLITE_OK){
    int cols = sqlite3_column_count(statement);
    int result = 0;
    while(true){

      result = sqlite3_step(statement);
			
      if(result == SQLITE_ROW){
	values.clear();;
	for(int col = 0; col < cols; col++){
	  val="";
	  char * ptr = (char*)sqlite3_column_text(statement, col);
	  if(ptr){
	    val = ptr;
	  }
	  else
	    val ="";
	  values.push_back(val);
	}

	results.push_back(values);
      }

      else{
	break;   
      }
    }
	   
    sqlite3_finalize(statement);
  }
	
  error = sqlite3_errmsg(base);
  if(error != "not an error"){
    std::vector<std::string> fail;
    fail.emplace_back(DATABASE_FAIL);
    fail.push_back(error);
    fail.push_back(query);
    std::cerr << query << " " << error << std::endl;
    results.clear();
    results.push_back(fail);
  }
  return results;  
}


void Database::close(){
  if(base!=NULL)
    sqlite3_close(base);
  else{std::cout<<"no close cus' null as u r"<<std::endl;}
}


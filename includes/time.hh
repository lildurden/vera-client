/**
 * \class Time
 *
 * \brief 
 *
 * \note 
 *
 * \author : Adrien Carteron$
 *
 * \version : 0.1 $
 *
 * \date : mer. févr. 18 10:30:19 CET 2015 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: mer. févr. 18 10:30:19 CET 2015
 *
 *
 */

#ifndef TIME_HH
#define TIME_HH

#include <string>
#include <fstream>
#include <ctime>

class Time{

private:
  unsigned int msec;
  unsigned int timestamp;
  unsigned int dailytimstmp;

  std::tm timestmp;
 
  void parse(std::string);
  void parse(std::string,std::string);

  bool greater(const Time&);
  bool less(const Time&);
  bool equal(const Time&);
  bool greater_equal(const Time&);
  bool less_equal(const Time&);
  Time& affect(const Time &);

  unsigned int plus(const Time&);
  unsigned int minus(const Time&);

  void to_timestamp();
  void to_time();
public:
  /** \brief Void constructor
   * 
   * add desc 
   * 
   */
  Time();
  Time(const Time &);
  Time(std::string );
  Time(std::string ,std::string);
  Time(std::time_t);
  /** \brief Destructor
   * 
   * add desc
   *
   */
  ~Time();

  unsigned long get_timestamp();
  unsigned int get_daily_timestamp();
  
  std::string to_string()const;
  
  bool operator>(const Time& );
  bool operator<(const Time& );
  bool operator==(const Time& );
  bool operator>=(const Time& );
  bool operator<=(const Time& );
  Time& operator=(const Time& );
  Time& operator=(std::string );
  unsigned int operator+(const Time& );
  unsigned int operator-(const Time& );
  Time& operator+=(const Time& );
  Time& operator-=(const Time& );

  unsigned int minus_day_(const Time& );
 
  
  std::string get_day_str() const;
  std::string get_hour_str()const;
  
  friend std::ostream& operator<<(std::ostream& out, const Time& a);
  
};



#endif // TIME_HH

/**
 * \class sensor
 *
 * \brief 
 *
 * \note 
 *
 * \author : Adrien Carteron$
 *
 * \version : 0.1 $
 *
 * \date : jeu. déc. 10 09:39:41 CET 2015 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: jeu. déc. 10 09:39:41 CET 2015
 *
 *
 */

#ifndef SENSOR_HH
#define SENSOR_HH

#include <string>
#include <memory>

#include "category.hh"
#include "room.hh"

class Sensor{

private:
  //from other class

  std::shared_ptr<Category> category;
  std::shared_ptr<Room> room;
  std::shared_ptr<Sensor> parent;
  
  int type;
 
  int id;
  std::string status;
  unsigned int timestamp;
  std::string name;
  

public:
  /** \brief Void constructor
   * 
   * add desc 
   * 
   */
  Sensor();
  Sensor(std::shared_ptr<Category>,
	 std::shared_ptr<Room>,
	 std::shared_ptr<Sensor>,
	 int,
	 std::string,unsigned int,std::string);
  /** \brief Destructor
   * 
   * add desc
   *
   */
  ~Sensor();

  void set_status(std::string, unsigned int);

  std::string get_status();
  unsigned int get_last_trip();

  bool compare(const Sensor&);

  Sensor& operator=(const Sensor &);

  int get_id();

};

#endif // SENSOR_HH

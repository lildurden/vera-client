/**
 * \class vector_sensors
 *
 * \brief 
 *
 * \note 
 *
 * \author : Adrien Carteron$
 *
 * \version : 0.1 $
 *
 * \date : jeu. déc. 10 13:47:42 CET 2015 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: jeu. déc. 10 13:47:42 CET 2015
 *
 *
 */

#ifndef VECTOR_SENSORS_HH
#define VECTOR_SENSORS_HH

#include <vector>
#include <memory>
#include "sensor.hh"

class Vector_Sensors{

private:
  std::vector<std::unique_ptr<Sensor>> sensors;
public:
  /** \brief Void constructor
   * 
   * add desc 
   * 
   */
  Vector_Sensors();
  /** \brief Destructor
   * 
   * add desc
   *
   */
  ~Vector_Sensors();

};

#endif // VECTOR_SENSORS_HH

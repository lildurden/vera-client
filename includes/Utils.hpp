/**
 * \class Utils
 *
 * \brief Contains some useful methods and tricks
 *
 * \note Accept somme things to be add in
 *
 * \author $Author: Adrien Carteron$
 *
 * \version $Revision: 0.5 $
 *
 * \date $Date: 2014/07/11 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: 2012/11/12
 *
 *
 */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <sstream>

class Utils{
private:
public:
  /** \brief A method to get current date and time as string.
   * \return Current date and time as string
   */
  static std::string getTime(){
    time_t now = time (NULL);

    /* la convertir en heure locale */
    struct tm tm_now = *localtime (&now);

    /* Creer une chaine JJ/MM/AAAA HH:MM:SS */
    char s_now[sizeof "JJMMAAAAHHMMSS"];

    strftime (s_now, sizeof s_now, "%d%m%Y%H%M%S", &tm_now);
    return toString(s_now);
  }
  /** \brief A method to convert any type of input in string
   * \param any type of data
   * \return data given as string
   * 
   * HowTo:
   * int truc=42;
   * string machin=Utils::toString<int>(truc);
   *
   */
  template <typename type>
  static std::string toString(type var){
    std::ostringstream ss;
    ss << var;
    return ss.str();
  }
  /** \brief A method to convert a string as any type of data
   * \param a data as string
   * \return data given as any type
   *
   * HowTo:
   * string machin="42";
   * int truc=Utils::stringTo<int>(machin);
   * 
   */
  template <class type>
  static type stringTo(std::string val){
    type ret;//=NULL;
    std::istringstream istr(val);    
    istr >> ret;
    return ret;
  }
};

#endif //UTILS_HPP

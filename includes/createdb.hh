#ifndef CREATEDB_HH
#define CREATEDB_HH
#include <vector>
#include <string>
class Create_DB{
private:
  
public:
  static std::vector<std::string> get_create_requests(){
    std::vector<std::string> requests;

    // requests.emplace_back("drop table application");
    // requests.emplace_back("drop table date_time");
    // requests.emplace_back("drop table device");
    // requests.emplace_back("drop table device_type");
    // requests.emplace_back("drop table device_location");
    // requests.emplace_back("drop table correspond");
    // requests.emplace_back("drop table log");

    requests.emplace_back("CREATE TABLE application (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,name TEXT NOT NULL, UNIQUE (name));");
    requests.emplace_back("CREATE TABLE device_type (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,type TEXT NOT NULL,UNIQUE (type));");
    requests.emplace_back("CREATE TABLE device_location (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,location TEXT NOT NULL,UNIQUE (location));");
    requests.emplace_back("CREATE TABLE device (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id_location INTEGER NOT NULL,id_type INTEGER NOT NULL,UNIQUE (id_type,id_location),FOREIGN KEY (id_type) REFERENCES device_type(id),FOREIGN KEY (id_location) REFERENCES device_location(id));");
    requests.emplace_back("CREATE TABLE date_time (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,dt datetime NOT NULL DEFAULT '0000-00-00 00:00:00.000',UNIQUE (dt));");
    requests.emplace_back("CREATE TABLE log (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id_date_time INTEGER NOT NULL,id_device INTEGER NOT NULL,status TEXT NOT NULL,UNIQUE (id_date_time,id_device,status),FOREIGN KEY (id_device) REFERENCES device(id),	FOREIGN KEY (id_date_time) REFERENCES date_time(id));");
    requests.emplace_back("CREATE TABLE correspond (id_log INTEGER NOT NULL, id_app INTEGER NOT NULL,PRIMARY KEY (id_log,id_app),FOREIGN KEY (id_log) REFERENCES log(id), FOREIGN KEY (id_app) REFERENCES application(id) );");

    return requests;
  }
};

#endif // CREATEDB_HH

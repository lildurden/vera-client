/**
 * \class room
 *
 * \brief 
 *
 * \note 
 *
 * \author : Adrien Carteron$
 *
 * \version : 0.1 $
 *
 * \date : jeu. déc. 10 11:22:05 CET 2015 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: jeu. déc. 10 11:22:05 CET 2015
 *
 *
 */

#ifndef ROOM_HH
#define ROOM_HH

#include "elements.hh"

class Room:public Elements{

private:
 

public:
  Room(std::string,unsigned int);
  

};

#endif // ROOM_HH

/**
 * \class elements
 *
 * \brief 
 *
 * \note 
 *
 * \author : Adrien Carteron$
 *
 * \version : 0.1 $
 *
 * \date : jeu. déc. 10 11:28:26 CET 2015 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: jeu. déc. 10 11:28:26 CET 2015
 *
 *
 */

#ifndef ELEMENTS_HH
#define ELEMENTS_HH

#include <string>

class Elements{

private:
  unsigned int id;
  std::string name;
public:

  Elements(std::string,unsigned int);
  ~Elements();

  unsigned int get_id();
  std::string get_name();
  Elements& operator=(const Elements&);

};

#endif // ELEMENTS_HH

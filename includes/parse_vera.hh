/**
 * \class parse_vera
 *
 * \brief 
 *
 * \note 
 *
 * \author : Adrien Carteron$
 *
 * \version : 0.1 $
 *
 * \date : ven. déc. 11 09:40:43 CET 2015 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: ven. déc. 11 09:40:43 CET 2015
 *
 *
 */

#ifndef PARSE_VERA_HH
#define PARSE_VERA_HH

#include <Poco/JSON/JSON.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Dynamic/Var.h>

#include <iostream>
#include <map>

#include "sensor.hh"
#include "Utils.hpp"
#include "request.hh"
#include "category.hh"
#include "room.hh"

class Parse_Vera{

private:
  std::map<int,std::shared_ptr<Category>> categories_map;
  std::map<int,std::shared_ptr<Room>> rooms_map;
  std::map<int,Sensor> sensors_map;
  Poco::JSON::Object::Ptr json_results;
  Request req;
  
  void set_rooms();
  void set_categories();
  //void set_devices();

  void set_cat4(Poco::JSON::Object::Ptr);
  void set_cat16(Poco::JSON::Object::Ptr);
  void set_cat17(Poco::JSON::Object::Ptr);
  void set_cat18(Poco::JSON::Object::Ptr);

  void set_cat4_1(Poco::JSON::Object::Ptr);
  void set_cat4_3(Poco::JSON::Object::Ptr,std::string);
public:
  void init(std::string);

  void set_all();
  void update_devices();
  void set_devices();
  void exec_request();
};

#endif // PARSE_VERA_HH

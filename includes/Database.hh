#ifndef DATABASE_HH
#define DATABASE_HH


#define DATABASE_FAIL "fail" /* database fail to do something */

#include <string>
#include <vector>

#include <sqlite3.h>
class Database{

private:
  sqlite3 * base;
  bool openDB;

  bool create_db(std::string);
  
  static int callback(void *data, int argc, char **argv, char **azColName){
    int i;
    fprintf(stderr, "%s: ", (const char*)data);
    for(i=0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
  }
public:
  /** \brief Void constructor
   * 
   * add desc 
   * 
   */
  Database();

  Database(std::string filename);

  /** \brief Destructor
   * 
   * add desc
   *
   */
  ~Database();
  bool open(std::string filename);

  // methode générique pour effectuer une requete sur la bdd
  std::vector<std::vector<std::string> > query(std::string query);
  void insert(std::string query);
  void close();
};

#endif // DATABASE_HH

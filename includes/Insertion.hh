/**
 * \class Insertion
 *
 * \note nothing to add
 *
 * \author $Author: Benjamin Sellam$
 *         $Revised by : Adrien Carteron$
 * \version $Revision: 1.0 $
 *
 * \date $Date: 2015/02/09 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: 2014/06/01
 *
 **/

#ifndef INSERTION_HH
#define INSERTION_HH

#include <vector>
#include <iostream>

#include "Database.hh"



using namespace std;

class Insertion{

private:

  Database db;

  std::vector<std::vector<std::string>> logs;
  std::vector<std::string> requests;

  
  
  
  std::vector<std::string> insert_line(std::string,std::string,std::string,std::string,std::string);
  std::string build_req_app(std::string);
  std::string build_req_dev_type(std::string);
  std::string build_req_dev_loc(std::string);
  std::string build_req_dev(std::string,std::string);
  std::string build_req_date_time(std::string);
  std::string build_req_log(std::string,std::string,std::string,std::string);
  std::string build_req_coresp(std::string,std::string,std::string,std::string);
public:

  
  // Constructeurs // Destructeur
  Insertion();
  ~Insertion();
  void init(std::string);
  void clear();
  void exec_requests();
  void get_file_content(std::string);
  void create_requests_db(std::string app_,std::string type_,std::string location_,std::string status_,std::string date_time_);


};


#endif //INSERTION_HH

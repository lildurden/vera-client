/**
 * \class category
 *
 * \brief 
 *
 * \note 
 *
 * \author : Adrien Carteron$
 *
 * \version : 0.1 $
 *
 * \date : jeu. déc. 10 11:22:14 CET 2015 $
 *
 * Contact: acarteron@openmailbox.org
 *
 * Created on: jeu. déc. 10 11:22:14 CET 2015
 *
 *
 */

#ifndef CATEGORY_HH
#define CATEGORY_HH


#include "elements.hh"

class Category:public Elements{

private:
 

public:

  Category(std::string,unsigned int);

};

#endif // CATEGORY_HH
